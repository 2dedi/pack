package pack

import (
	"time"
)

type Pack struct {
	before_sleep         int
	after_sleep          int
	after_response_sleep int
	time_wait            int
	answer_wait          int
	cmd                  []byte
	ident                string
	Year                 int
	Month                int
	Day                  int
	Hour                 int
	Min                  int
	Sec                  int
}

func New() *Pack {
	return &Pack{answer_wait: 1}
}

func (p *Pack) SetCmd(cmd []byte) {
	p.cmd = cmd
}

func (p *Pack) SetIdent(ident string) {
	p.ident = ident
}

func (p *Pack) GetBeforeSendSleep() time.Duration {
	return time.Duration(int64(p.before_sleep))
}

func (p *Pack) GetCmd() []byte {
	return p.cmd
}

func (p *Pack) GetIdent() string {
	return p.ident
}

func (p *Pack) GetAfterSendSleep() time.Duration {
	return time.Duration(int64(p.after_sleep))
}

func (p *Pack) GetAnswerWait() int {
	return p.answer_wait
}

func (p *Pack) SetAnswerWait(i int) {
	p.answer_wait = i
}

func (p *Pack) SetBeforeSleep(i int) {
	p.before_sleep = i
}

func (p *Pack) GetAfterResponseSleep() time.Duration {
	return time.Duration(int64(p.after_response_sleep))
}

func (p *Pack) SetAfterResponseSleep(i int) {
	p.after_response_sleep = i
}

func (p *Pack) SetAfterSendSleep(i int) {
	p.after_sleep = i
}
